function loadData() {
    return new Promise((resolve, reject) => {
      setTimeout(resolve, 2000);
    })
  }
  
  loadData()
    .then(() => {
      let preloaderEl = document.getElementById('preloader');
      preloaderEl.classList.add('hidden');
      preloaderEl.classList.remove('visible');
    });



const  new_friend = document.querySelectorAll('.new_friend_box');

new_friend.forEach(element => {
    element.addEventListener('click', function(){
        element.classList.toggle('new_friend_box_choosen');
    })
    element.addEventListener('mouseleave', function(e){
      if(element.classList.contains("new_friend_box_choosen")){
        element.classList.add('new_friend_box_remove');
      }
    })
});
