from modeltranslation.translator import TranslationOptions, translator

from .models import Interest, VkGroup, SocialProfile, Place


class InterestTranslationOptions(TranslationOptions):
    fields = ('name',)


class PlaceTranslationOptions(TranslationOptions):
    fields = ('name',)


translator.register(Interest, InterestTranslationOptions)
translator.register(Place, PlaceTranslationOptions)
