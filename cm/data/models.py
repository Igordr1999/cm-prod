from django.db import models
from django.utils.translation import ugettext_lazy as _
from imagekit.models import ProcessedImageField
from django.conf import settings

from .vk import VK


class Interest(models.Model):
    """!
        Interests of people

        This model stores the data of interests
        @param name interest name
        @param rating our rating value
    """
    name = models.CharField(max_length=64, verbose_name=_('Name'))
    rating = models.IntegerField(default=0, verbose_name=_('Rating'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Interest')
        verbose_name_plural = _('Interests')
        ordering = ["-rating", "name"]


class VkGroup(models.Model):
    """!
        Social group on VK social network

        This model stores the data of social group
        @param name name of social group
        @param group_id ID group on VK social network
        @param interest This field indicates the category of the group
        @param rating our rating value
    """

    name = models.CharField(max_length=64, verbose_name=_('Name'))
    group_id = models.IntegerField(unique=True, verbose_name=_('VK group id'))
    interest = models.ForeignKey(Interest, on_delete=models.CASCADE, verbose_name=_('Interest'))
    rating = models.IntegerField(default=0, verbose_name=_('Rating'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('VK group')
        verbose_name_plural = _('VK groups')
        ordering = ["-rating", "name"]


class SocialProfile(models.Model):
    """!
        Social profile

        This model stores the detailed information about people
        @param owner profile owner
        @param username username
        @param first_name first name
        @param last_name last name
        @param birthday day of birth
        @param bio biography
        @param vk_id VK user ID
        @param record_create_date create date
        @param record_update_date update date
        @param avatar avatar image
        @param avatar_url VK avatar url
        @param groups array of user groups
        @param interests array of user interests
        @param rating our rating value
    """

    owner = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_('Owner'))
    username = models.CharField(max_length=32, verbose_name=_('Username'))
    first_name = models.CharField(max_length=32, verbose_name=_('First name'))
    last_name = models.CharField(max_length=32, verbose_name=_('Last name'))
    birthday = models.DateTimeField(verbose_name=_('Birthday'))
    bio = models.CharField(max_length=256, verbose_name=_('Biography'))
    vk_id = models.IntegerField(verbose_name=_('VK ID'))
    record_create_date = models.DateTimeField(auto_now_add=True, verbose_name=_('Record create date'))
    record_update_date = models.DateTimeField(auto_now=True, verbose_name=_('Record update date'))
    avatar = ProcessedImageField(upload_to='data/socialprofile/avatar/',
                                 format='PNG',
                                 verbose_name=_('Photo'))
    avatar_url = models.URLField(null=True, blank=True, verbose_name=_('Photo URL'))
    groups = models.ManyToManyField(VkGroup, verbose_name=_('Groups'))
    interests = models.ManyToManyField(Interest, verbose_name=_('Interests'))
    rating = models.IntegerField(default=0, verbose_name=_('Rating'))

    def save(self, *args, **kwargs):
        from django.core.files.temp import NamedTemporaryFile
        import shutil
        import requests
        import uuid

        if self.avatar_url is not None:
            response = requests.get(self.avatar_url, stream=True)
            img_temp = NamedTemporaryFile()
            shutil.copyfileobj(response.raw, img_temp)
            random_name = uuid.uuid4().hex + ".png"
            self.avatar.save(random_name, img_temp, save=False)
        # now image data are in img_temp, how to pass that to ProcessedImageField?

        super(SocialProfile, self).save(*args, **kwargs)

    def save_groups(self):
        groups = VK().get_user_groups(user_id=self.vk_id, fields=['activity'], count=10)
        self.interests.clear()
        for group in groups:
            interest_name = group['activity']
            interest, created = Interest.objects.get_or_create(name=interest_name)
            self.interests.add(interest)

            # todo: get_or_create -> create_or_update
            vk_group, created = VkGroup.objects.get_or_create(name=group['name'], group_id=group['id'],
                                                              interest=interest)
            self.groups.add(vk_group)

    @classmethod
    def get_avatar_url(cls, vk_id):
        return VK().get_user_photos(owner_id=vk_id, count=1)[0]['sizes'][0]['url']

    @classmethod
    def get_similarity_rate(cls, my_profile, another_profile):
        return len(my_profile.interests.all() & another_profile.interests.all())

    @classmethod
    def get_similarity_interests(cls, my_profile, another_profile):
        return my_profile.interests.all() & another_profile.interests.all()

    @classmethod
    def get_similarity_with_all(cls, my_profile, limit=4):
        similarity_array = list()
        for another in SocialProfile.objects.all():
            if another == my_profile:
                continue
            sim_rate = cls.get_similarity_rate(my_profile=my_profile, another_profile=another)
            similarity_array.append([sim_rate, another.id])
        print(similarity_array)
        similarity_array = sorted(similarity_array, reverse=True)
        print(similarity_array)

        top_similarity = list()
        for i in similarity_array:
            if len(top_similarity) >= limit:
                break
            else:
                top_similarity.append(SocialProfile.objects.get(id=i[1]))
        return top_similarity

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    class Meta:
        verbose_name = _('Social profile')
        verbose_name_plural = _('Social profiles')
        ordering = ["-rating"]


class Place(models.Model):
    """!
            Top places by interest

            This model stores the detailed of Moscow top placeы
            @param name name of place
            @param latitude latitude coordinate
            @param longitude longitude coordinate
            @param interest user interest
            @param rating our rating value
            @param photo top photo of place
        """
    name = models.CharField(max_length=64, verbose_name=_('Name'))
    latitude = models.FloatField(verbose_name=_('Latitude'))
    longitude = models.FloatField(verbose_name=_('Longitude'))
    interest = models.ForeignKey(Interest, on_delete=models.CASCADE, verbose_name=_('Interest'))
    rating = models.IntegerField(default=0, verbose_name=_('Rating'))
    photo = ProcessedImageField(upload_to='data/place/photo/',
                                format='PNG',
                                verbose_name=_('Photo'), null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Place')
        verbose_name_plural = _('Places')
        ordering = ["-rating"]


class Match(models.Model):
    """!
        Match model

        This model stores the detailed information about match and prospective_locations
        @param user1 This profile liked the user2
        @param user2 This profile received a like from user1
        @param is_mutual Is there mutual sympathy?
        @param prospective_locations Interesting places for people based on common interests
    """
    user1 = models.ForeignKey(SocialProfile, on_delete=models.CASCADE, related_name='user1', verbose_name=_('User 1'))
    user2 = models.ForeignKey(SocialProfile, on_delete=models.CASCADE, related_name='user2', verbose_name=_('User 2'))
    is_mutual = models.BooleanField(default=False, verbose_name=_('Mutual'))
    prospective_locations = models.ManyToManyField(Place, verbose_name=_('Prospective locations'))

    def __str__(self):
        return "{} - {}".format(self.user1, self.user2)

    class Meta:
        verbose_name = _('Match')
        verbose_name_plural = _('Matches')

    @classmethod
    def create_or_mutual(cls, user1, user2):
        try:
            # Mutual
            m = Match.objects.get(user1=user2, user2=user1)
            m.is_mutual = True
            m.save()
            cls.create_prospective_locations(match=m)
        except Match.DoesNotExist:
            try:
                # Do not double one-way match
                m = Match.objects.get(user1=user1, user2=user2)
            except Match.DoesNotExist:
                # One-way match
                Match.objects.create(user1=user1, user2=user2)

    @classmethod
    def create_prospective_locations(cls, match):
        my_profile, another_profile = match.user1, match.user2
        interests = SocialProfile.get_similarity_interests(my_profile, another_profile)
        for interest in interests:
            places_by_interest = Place.objects.filter(interest=interest)
            match.prospective_locations.add(*places_by_interest)
