from rest_framework import serializers

from .models import Interest, VkGroup, SocialProfile, Place, Match


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = ('name', 'rating')


class VkGroupSerializer(serializers.ModelSerializer):
    interest = InterestSerializer()

    class Meta:
        model = VkGroup
        fields = ('name', 'group_id', 'interest', 'rating')


class SocialProfileSerializer(serializers.ModelSerializer):
    interests = InterestSerializer(many=True)

    class Meta:
        model = SocialProfile
        fields = ('id', 'username', 'first_name', 'last_name', 'birthday', 'bio', 'vk_id', 'avatar', 'interests',
                  'rating')


class PlaceSerializer(serializers.ModelSerializer):
    interest = InterestSerializer()

    class Meta:
        model = Place
        fields = ('name', 'latitude', 'longitude', 'interest', 'rating', 'photo')


class MatchSerializer(serializers.ModelSerializer):
    user1 = SocialProfileSerializer()
    user2 = SocialProfileSerializer()
    prospective_locations = PlaceSerializer(many=True)

    class Meta:
        model = Match
        fields = ('user1', 'user2', 'is_mutual', 'prospective_locations')
