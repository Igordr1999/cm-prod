from django import forms
import django_filters
from .models import SocialProfile


class SocialProfileForm(forms.ModelForm):
    class Meta:
        model = SocialProfile
        fields = ['username', 'first_name', 'last_name', 'birthday', 'bio', 'vk_id']
