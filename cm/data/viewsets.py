from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, generics

from .models import Interest, VkGroup, SocialProfile, Place, Match
from .serializers import InterestSerializer, VkGroupSerializer, SocialProfileSerializer, PlaceSerializer, MatchSerializer


class InterestListView(generics.ListAPIView):
    queryset = Interest.objects.all()
    serializer_class = InterestSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('name', 'rating')
    ordering_fields = ('name', 'rating')
    search_fields = ('name', 'rating')


class VkGroupListView(generics.ListAPIView):
    queryset = VkGroup.objects.all()
    serializer_class = VkGroupSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('name', 'group_id', 'interest', 'rating')
    ordering_fields = ('name', 'group_id', 'interest', 'rating')
    search_fields = ('name', 'group_id', 'interest', 'rating')


class SocialProfileListView(generics.ListAPIView):
    queryset = SocialProfile.objects.all()[:4]
    serializer_class = SocialProfileSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('username', 'first_name', 'last_name', 'birthday', 'bio', 'vk_id',
                     'interests', 'rating')
    ordering_fields = ('username', 'first_name', 'last_name', 'birthday', 'bio', 'vk_id',
                       'interests', 'rating')
    search_fields = ('username', 'first_name', 'last_name', 'birthday', 'bio', 'vk_id',
                     'interests', 'rating')


class PlaceListView(generics.ListAPIView):
    queryset = Place.objects.all()
    serializer_class = PlaceSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('name', 'latitude', 'longitude', 'interest', 'rating')
    ordering_fields = ('name', 'latitude', 'longitude', 'interest', 'rating')
    search_fields = ('name', 'latitude', 'longitude', 'interest', 'rating')


class MatchListView(generics.ListAPIView):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
    filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
    filter_fields = ('user1', 'user2', 'is_mutual', 'prospective_locations')
    ordering_fields = ('user1', 'user2', 'is_mutual', 'prospective_locations')
    search_fields = ('user1', 'user2', 'is_mutual', 'prospective_locations')
