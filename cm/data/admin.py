from django.contrib import admin
from modeltranslation.admin import TranslationAdmin

from .models import Interest, VkGroup, SocialProfile, Place, Match


@admin.register(Interest)
class InterestAdmin(TranslationAdmin):
    list_display = ['name_en', 'name_ru', 'rating']


@admin.register(VkGroup)
class VkGroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'group_id', 'interest', 'rating']


@admin.register(SocialProfile)
class SocialProfileAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'avatar_url', 'rating']


@admin.register(Place)
class PlaceAdmin(admin.ModelAdmin):
    list_display = ['name', 'latitude', 'longitude', 'rating']


@admin.register(Match)
class MatchAdmin(admin.ModelAdmin):
    list_display = ['user1', 'user2', 'is_mutual']
