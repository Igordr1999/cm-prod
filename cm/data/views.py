from django.http import HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404, render
from django.utils.translation import LANGUAGE_SESSION_KEY
from django.urls import reverse_lazy
from profiles.models import Profile
from django.views.generic.edit import FormView, CreateView, UpdateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.decorators import method_decorator
from django.contrib.auth.mixins import LoginRequiredMixin

from django_filters.views import FilterView

from .models import SocialProfile, Match
from .forms import SocialProfileForm


class EditSocialProfileView(LoginRequiredMixin, CreateView):
    form_class = SocialProfileForm
    template_name = 'data/social_profile.html'
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form_url = form.save(commit=False)
        form_url.owner = Profile.objects.get(id=self.request.user.id)
        form_url.avatar_url = SocialProfile.get_avatar_url(form_url.vk_id)
        form_url.save()

        social_obj = SocialProfile.objects.get(owner_id=self.request.user.id)
        social_obj.save_groups()

        return super().form_valid(form)


def set_match(request, user1_id, user2_id):
    user1 = SocialProfile.objects.get(id=user1_id)
    user2 = SocialProfile.objects.get(id=user2_id)

    Match.create_or_mutual(user1=user1, user2=user2)

    return HttpResponse("OK")


def index(request):
    return render(request, 'data/index.html')


def find(request):
    return render(request, 'data/find.html')


def prof_guest(request):
    return render(request, 'data/prof_guest.html')


def prof_local(request):
    return render(request, 'data/prof_local.html')


def profile(request):
    return render(request, 'data/profile.html')
