from django.urls import path

from . import views
from . import viewsets
from rest_framework.authtoken import views as drf

urlpatterns = [
    path('social-profile/', views.EditSocialProfileView.as_view(), name="social_profile"),
    path('api/InterestListView/', viewsets.InterestListView.as_view()),
    path('api/VkGroupListView/', viewsets.VkGroupListView.as_view()),
    path('api/SocialProfileListView/', viewsets.SocialProfileListView.as_view()),
    path('api/PlaceListView/', viewsets.PlaceListView.as_view()),
    path('api/MatchListView/', viewsets.MatchListView.as_view()),
    path('api/GetToken/', drf.obtain_auth_token, name="get_token"),
    path('api/SetMatch/<int:user1_id>/<int:user2_id>/', views.set_match, name='set_match'),

    path('', views.index, name='index'),
    path('find/', views.find, name='find'),
    path('prof_guest/', views.prof_guest, name='prof_guest'),
    path('prof_local/', views.prof_local, name='prof_local'),
    path('profile/', views.profile, name='profile'),
]
