const reg_form_show_btn = document.querySelector('.reg_form_show_btn');
const sing_form_show_btn = document.querySelector('.sing_form_show_btn');
const sing_form = document.querySelector('.sing_form');
const reg_form = document.querySelector('.reg_form');
const content = document.querySelector('.content');
const close_btn = document.querySelector('.close_btn');
const reg_btn = document.querySelector('.reg_btn');
const bg_img = document.querySelector('.bg_img img');
const check_role = document.querySelector('.check_role');
const check_role1 = document.querySelector('.check_role1');

reg_btn.onclick = function(e){
    e.preventDefault();

    if (check_role.checked == true){
        window.location.href = "pages/prof_guest.html";
    }else if(check_role1.checked == true){
        window.location.href = "pages/prof_local.html";
    }
}

reg_form_show_btn.addEventListener('click', function(){
    reg_form.classList.add('form_show');
    content.style.opacity = 0;
    close_btn.style.visibility = "visible";
    bg_img.style.opacity = 0.03;
})

sing_form_show_btn.addEventListener('click', function(){
    sing_form.classList.add('form_show');
    content.style.opacity = 0;
    close_btn.style.visibility = "visible";
    bg_img.style.opacity = 0.03;
})

close_btn.addEventListener('click', function(){
    content.style.opacity = 1;
    reg_form.classList.remove('form_show');
    sing_form.classList.remove('form_show');
    close_btn.style.visibility = "hidden";
    bg_img.style.opacity = 0.1;
})




    